<!-- # Custom-CF-README-documents -->
<!-- All the README documents that will be used as a CF hosted script -->
<!-- Original file found here: https://github.com/PotatoMindPopper/Custom-CF-README-documents/blob/main/Party/Vrijdagmiddag-feest/README.md -->

# Vrijdagmiddag 12-11-2021 <br />
Op vrijdagmiddag zal vanaf 13:00 het feest al beginnen. <br />
Dit gebeurt waarschijnlijk in het Gorlaeus Collegezalengebouw, gevestigd aan de Einsteinweg 55 te Leiden. <br />
Vanaf daar zullen een groot deel van de studenten / aanwezigen met de fiets naar Leiden Station, de zijde van het LUMC / Leiden Bio-Science park. <br />
Hier zal men de fiets in de stalling plaatsen en vervolgens naar het perron lopen, waar de trein naar Amsterdam Centraal vertrekt, via Haarlem Centraal.  <br />

## Vertrek van Leiden Centraal <br />
Op Haarlem Centraal aangekomen te zijn, zal de groep de bus pakken naar de eerst volgende halte, het Verwulft / Haarlem Centrum.  <br />
Hier zal de groep uitstappen en naar een lunch locatie gaan, om vervolgens na het lunchen een klein stukje van Haarlem zelf te bekijken.  <br />
  Dit zal voornamelijk de Grote Markt zijn en een stukje langs het Spaarne. <br />
Na een deel van Haarlem gezien te hebben, zal de groep dezelfde bus weer instappen, bij het Verwulft / Haarlem Centrum.   <br />

## Vervolg na de lunch in Haarlem  <br />
Deze zal doorrijden tot aan de halte Vijfhuizen / Vijfhuizen, waar de groep uit zal stappen.   <br />
De groep zal te voet naar het huis lopen, om daar ontvangen te worden met een drankje.  <br />
  Eenmaal neergedaald, kan er gekozen worden om of een rondje door het dorp te maken of gelijk het gereedschap naar beneden te halen. <br />
    Als er gekozen wordt voor een rondje door het dorp, dan zullen de voornaamste plekken **het MH17-monument** zijn en het **Fort van Vijfhuizen**. <br />
  Als er gekozen wordt voor het gereedschap, dan gaat men lekker een beetje chillen en een spelletje spelen.  <br />
    Dit zal op PC mogelijk zijn en op PS4, waarbij dit de **voornaamste spellen** zijn:  <br />
      1.  GTA V             (modmenu available)         (PC & PS4)  <br />
      2.  Krunker.io                                    (PC)        <br />
      3.  Rainbow Six Siege (no rank available yet)     (PC & PS4)  <br />
      4.  CS:GO             (no rank available yet)     (PC)        <br />
      5.  Rocket League     (no rank available)         (PC & PS4)  <br />
      6.  Apex Legends      (no rank available yet)     (PC & PS4)  <br />
      7.  Call of Duty:                                             <br />
        -  Black Ops III / IIII                         (PS4)       <br />
        -  Modern Warfare                               (PC)        <br />
        -  Modern Warfare II Remasterd                  (PS4)       <br />
      8.  Portal 1 / 2                                  (PC)        <br />
      9.  ETS2 / Planet Coaster / Cities Skylines       (PC)        <br />
      10. The Henry Stickmin Collection                 (PC)        <br />
      11. Battlefield 1                                 (PC & PS4)  <br />
      12. Need For Speed:                                           <br />
        -  Heat                                         (PC)        <br />
        -  Rivals                                       (PC)        <br />
     
Om ervoor te zorgen dat men niet uithongerd, zal er tussentijds verschillende hapjes beschikbaar zijn.  <br />
  Zo hebben we sowieso chips, nog meer taart en misschien hebben we ook Bittergarnituur.  <br />

## Richting de avondklok <br />
Zodra het richting 18:00 gaat, zal er eten besteld worden, wat waarschijnlijk rond is en volgens ons uit Italië komt. <br />
Tevens zal er een dessert beschikbaar zijn na de maaltijd, wat waarschijnlijk koud is en volgens ons uit water komt.  <br />
Na het eten, kan men zelf bepalen wat hij gaat doen, dit kan bijvoorbeeld verder gaan met gamen zijn, of alweer richting huis willen. <br />
  Wanneer er voor richting huis gekozen wordt, zal er gekeken worden naar wat het handigste en dichtsbijzijnde treinstation is. <br />
    De volgende **stations** komen sowieso in aanmerking: <br />
      1.  Haarlem Station       <br />
      2a. Heemstede-Aerdenhout  <br />
      2b. Haarlem-Spaarnwoude   <br />
      3.  Amsterdam-Sloterdijk  <br />
      4.  Amsterdam-Zuid        <br />
      5.  Schiphol Airport      <br />
      6.  Hoofddorp Station     <br />
      7.  Nieuw-Vennep Station  <br />
    
Dit is tot nu het feestje. <br />

###### Suggetions  <br />
Als er nog vragen zijn of suggesties / tips die je nog kwijt wilt, dan kun je de **eigenaar van dit account** contacteren via deze [link](https://linksome.me/t3ch)
